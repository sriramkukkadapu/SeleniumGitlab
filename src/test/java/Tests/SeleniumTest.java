package Tests;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

//import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumTest {
  @Test
  public void test1() {
	  
	  System.out.println("Selenium Test - 1 Launch browser and assert title");
	  
	  //WebDriverManager.chromedriver().setup();
	  
	  ChromeOptions opt = new ChromeOptions();
	  opt.setHeadless(true);
	  
	  WebDriver driver=new ChromeDriver(opt);
	  
	  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	  driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
	  
	  driver.get("https://www.google.co.in");
	  
	  System.out.println("Page title: "+driver.getTitle());
	  
	  Assert.assertEquals(driver.getTitle(), "Google");
	  
	  
	  driver.quit();
	  
	  
  }
  
}
